# Desafio - Estacionamento de Carros

Esta API foi desenvolvida como parte de um desafio. O objetivo é criar um sistema para gerenciamento de um estacionamento.

## Funcionalidades:

- Obter um relatório geral do estacionamento;
- Consultar quantidade de vagas disponíveis;
- Listar posição das vagas disponíveis;
- Estacionar o veículo em vaga disponível;
- Pagamento do ticket;
- Relatório gerencial com quantidade de veículos estacionados e somatório dos pagamentos realizados (apenas dados em formato de JSON);
- Para mais funcionalidades, consultar lista de endpoints mais abaixo.

## Testando a aplicação online:

A fim de facilitar o teste da aplicação, foi feito um deploy da aplicação no Heroku. 

O deploy pode ser acessado no seguinte endereço: https://sistemaestacionamento.herokuapp.com/ 

Para utilizar os endpoints da aplicação para teste, basta inserir os endereços listados mais abaixo no final da URL, como por exemplo: 
"https://sistemaestacionamento.herokuapp.com/estacionamento", que irá retornar um JSON com todos os dados do sistema. 

## Instalando a aplicação localmente:

A aplicação possui arquivos de configuração de ambientes estanciados. São 3 ambientes: test, dev e prod. 

Por padrão, a aplicação vem com o ambiente test habilitado, onde ela irá rodar a aplicação via banco H2. Portanto, para executar a aplicação, basta importar ela no STS (através do arquivo.zip ou de um git clone no repositório no GitLab), e iniciar. As dependências estão gerenciadas pelo Maven, então você não deve ter problemas com isso. 

## Configurando o banco da aplicação:

O H2 irá zerar sua base sempre que a execução for encerrada. Para evitar isto, é necessário mudar o ambiente para dev. Para mudar o ambiente, basta acessar o arquivo src\main\resources\application.properties e alterar a variável de ambiente para uma de sua preferência.

O ambiente dev irá usar por padrão o banco MySql. Você irá precisar atualizar os dados do banco instalado na sua máquina no arquivo src\main\resources\application-dev.properties. Na primeira vez que você rodar a aplicação, você deverá atualizar a variável 'spring.jpa.hibernate.ddl-auto' para create, a fim de estanciar o banco de dados e efetuar o povoamento do banco com o estacionamento e com as vagas vinculadas a ele. Uma vez estanciado o banco, basta mudar o valor da variável para o padrão, none. 

## Observação

Para fins de teste, a aplicação simula uma cobrança por minuto, ao invés de horas. Para mudar isto, basta configurar o arquivo src\main\java\domain\Pagamento.java, segundo as instruções comentadas no arquivo. 

## CI/CD

Para implantação de um sistema CI/CD, foram utilizados uma configuração de um pipeline no GitLab. Esta configuração pode ser verificada no arquivo .gitlab-ci.yml, localizado na raiz da aplicação. Ou diretamente no repositório do projeto, no link: https://gitlab.com/mateusdml/sistema-estacionamento

## Endpoints

A aplicação possui os seguintes endpoints REST:

### Estacionamento:

/estacionamento (GET) => Retorna o estacionamento padrão do sistema (1)

/estacionamento/estacionar/{id}/{placaCarro} (PUT) => Estaciona um carro em uma vaga disponível (Parâmetros=ID da vaga (Deve existir). Placa do carro)

/estacionamento/vagar/{id} (PUT) => Libera uma vaga e gera um novo ticket para pagamento (Parâmetros=ID da vaga (Deve existir e estar Ocupada))

/estacionamento/pagar/{id} (PUT) => Realiza o pagamento de um ticket (Parâmetros=ID do ticket)

### Faturamento:

/faturamento(GET) => Retorna a lista de pagamentos cadastrados no sistema

/faturamento/{id} (GET) => Busca um pagamento específico pelo ID (Parâmetros=ID do pagamento)

/faturamento/pagos (GET) => Retorna todos os pagamentos com estado "Pago"

/faturamento/pendentes (GET) => Retorna todos os pagamentos com estado "Pendente"

/faturamento (POST) => Insere manualmente um novo pagamento no sistema.

/faturamento {id} (PUT) => Atualiza manualmente um pagamento (Parâmetros=ID da pagamento)

/faturamento/{id} (DELETE) => Busca e deleta um pagamento do sistema através do ID (Parâmetros=ID do pagamento)

/faturamento/page (GET) => Retorna todos os pagamentos paginados. (Valores padrão: page=0, linesPerPage=10, orderby=id, direction=ASC)

### Vaga

/vagas (GET) => Retorna a lista de vagas cadastrados no sistema

/vagas/{id} (GET) => Busca uma vaga específica pelo ID (Parâmetros=ID da vaga)

/vagas/disponiveis (GET) => Retorna uma lista com todas as vagas disponíveis

/vagas/ocupadas (GET) => Retorna uma lista com todas as vagas ocupadas

/vagas/quantidadedisponiveis (GET) => Retorna um inteiro com o número de vagas disponiveis

/vagas (POST) => Insere uma nova vaga no sistema

/vagas/{id} (PUT) => Atualiza manualmente uma vaga (Parâmetros=ID da vaga)

/vagas/{id} (DELETE) Busca e deleta uma vaga do sistema através do ID (Parâmetros=ID da vaga)