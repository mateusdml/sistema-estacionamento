package com.mateus.estacionamento.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mateus.estacionamento.domain.Vaga;

public interface VagaRepository extends JpaRepository<Vaga, Integer> {

}
