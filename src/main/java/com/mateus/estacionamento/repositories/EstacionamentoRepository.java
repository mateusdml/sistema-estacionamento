package com.mateus.estacionamento.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mateus.estacionamento.domain.Estacionamento;

public interface EstacionamentoRepository extends JpaRepository<Estacionamento, Integer> {

}
