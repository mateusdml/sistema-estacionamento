package com.mateus.estacionamento.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mateus.estacionamento.domain.Pagamento;

public interface PagamentoRepository extends JpaRepository<Pagamento, Integer> {

}
