package com.mateus.estacionamento;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SistemaEstacionamentoApplication implements CommandLineRunner {
	
	//Inicia a aplicação
	public static void main(String[] args) {
		SpringApplication.run(SistemaEstacionamentoApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
	}

}
