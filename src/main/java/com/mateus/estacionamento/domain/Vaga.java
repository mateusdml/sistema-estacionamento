package com.mateus.estacionamento.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Vaga implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//Classe Vaga e mapeamento relacional
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private String placaCarro;
	private Boolean ocupada;
	private String codVaga;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="estacionamento_id")
	private Estacionamento estacionamento;
	
	@JsonFormat(pattern="dd/MM/yyyy HH:mm")
	private Date horaDeEntrada; 
	
	public Vaga () {
		
	}
	
	public Vaga(Integer id, Estacionamento estacionamento, String codVaga) {
		super();
		this.id = id;
		this.estacionamento = estacionamento;
		this.codVaga = codVaga;
		this.ocupada = false;
	}

	public Vaga(Integer id) {
		super();
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPlacaCarro() {
		return placaCarro;
	}

	public void setPlacaCarro(String placaCarro) {
		this.placaCarro = placaCarro;
	}

	public Date getHoraDeEntrada() {
		return horaDeEntrada;
	}

	public void setHoraDeEntrada(Date horaDeEntrada) {
		this.horaDeEntrada = horaDeEntrada;
	}
	
	public Boolean getOcupada() {
		return ocupada;
	}

	public void setOcupada(Boolean ocupada) {
		this.ocupada = ocupada;
	}
	
	public String getCodVaga() {
		return codVaga;
	}

	public void setCodVaga(String codVaga) {
		this.codVaga = codVaga;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vaga other = (Vaga) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
