package com.mateus.estacionamento.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Estacionamento implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//Classe estacionamento e mapeamento relacional
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@OneToMany(mappedBy="estacionamento")
	private List<Vaga> vagas = new ArrayList<>();
	
	@OneToMany(mappedBy = "estacionamento")
	private List<Pagamento> pagamentos = new ArrayList<>();
	
	public Estacionamento () {
		
	}
	
	public Estacionamento(Integer id, List<Vaga> vagas) {
		super();
		this.id = id;
		this.vagas = vagas;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Vaga> getVagas() {
		return vagas;
	}
	
	public void setVagas(List<Vaga> vagas) {
		this.vagas = vagas;
	}
	
	public List<Pagamento> getPagamentos() {
		return pagamentos;
	}

	public void setPagamentos(List<Pagamento> pagamentos) {
		this.pagamentos = pagamentos;
	}
	
	public void addPagamento(Pagamento pag) {
		this.pagamentos.add(pag);
	}

}
