package com.mateus.estacionamento.domain;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mateus.estacionamento.domain.enums.EstadoPagamento;


@Entity
public class Pagamento implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//Classe pagamento e mapeamento relacional
	
	//Define o tempo de cobrança no estacionamento 
	//(180000 = Cobra a cada 3min) (10800000‬ = Cobra a cada 3h)
	static Long tempoCobrança = Long.valueOf(180000);
	static Long horaExtra = Long.valueOf(60000);
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private Integer estado;
	private String codVaga;
	private String placaCarro;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="estacionamento_id")
	private Estacionamento estacionamento;
	
	@JsonFormat(pattern="dd/MM/yyyy HH:mm")
	private Date horaDeEntrada;	
	
	@JsonFormat(pattern="dd/MM/yyyy HH:mm")
	private Date horaDeSaída;	
	private Integer valor;
	
	@JsonFormat(pattern="dd/MM/yyyy HH:mm")
	private Date dataPagamento; 
	
	public Pagamento () {
		
	}

	public Pagamento(Vaga vaga, Estacionamento estacionamento) {
		super();
		this.id = null;
		this.estado = EstadoPagamento.PENDENTE.getCod();
		this.codVaga = vaga.getCodVaga();
		this.placaCarro = vaga.getPlacaCarro();
		this.horaDeEntrada = vaga.getHoraDeEntrada();
		this.horaDeSaída = new java.util.Date();
		this.valor = calcularValor(new java.util.Date(), vaga.getHoraDeEntrada());
		this.dataPagamento = null;
		this.estacionamento = estacionamento;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public EstadoPagamento getEstado() {
		return EstadoPagamento.toEnum(estado);
	}

	public void setEstado(EstadoPagamento estado) {
		this.estado = estado.getCod();
	}

	public String getCodVaga() {
		return codVaga;
	}

	public void setCodVaga(String codVaga) {
		this.codVaga = codVaga;
	}

	public String getPlacaCarro() {
		return placaCarro;
	}

	public void setPlacaCarro(String placaCarro) {
		this.placaCarro = placaCarro;
	}	

	public Date getHoraDeSaída() {
		return horaDeSaída;
	}

	public void setHoraDeSaída(Date horaDeSaída) {
		this.horaDeSaída = horaDeSaída;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}
	
	//Calcula o valor da tarifa a partir da data de entrega e data de saída(Detectada automaticamente pelo sistema)
	private Integer calcularValor(Date date, Date horaDeEntrada) {
		Long time = (date.getTime() - horaDeEntrada.getTime());
		if(time<=tempoCobrança) {
			return 7;
		} else {
			Integer total = 7;
			time -= tempoCobrança;
			while (time>0) {
				total += 3;
				time -= horaExtra;
			}
			return total;
		}
	}
}
