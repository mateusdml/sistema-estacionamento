package com.mateus.estacionamento.resources;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mateus.estacionamento.domain.Vaga;
import com.mateus.estacionamento.services.VagaService;

@RestController
@RequestMapping(value="vagas")
public class VagaResource {
	
	//Disponibiliza endpoints de vagas para interação com o sistema
	
	@Autowired
	private VagaService service;
	
	//Retorna a lista de vagas cadastrados no sistema
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<Vaga>> findAll() {
		List<Vaga> vagas = service.findAll();
		return ResponseEntity.ok().body(vagas);
	}
	
	//Busca uma vaga específica pelo ID
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<Vaga> find(@PathVariable("id") Integer id){
		return ResponseEntity.ok().body(service.find(id));
	}
	
	//Retorna uma lista com todas as vagas disponíveis
	@RequestMapping(value="/disponiveis", method=RequestMethod.GET)
	public ResponseEntity<List<Vaga>> getVagasDisponiveis(){
		List <Vaga> vagas = service.getVagasDisponiveis();
		return ResponseEntity.ok().body(vagas);
	}
	
	//Retorna uma lista com todas as vagas ocupadas
	@RequestMapping(value="/ocupadas", method=RequestMethod.GET)
	public ResponseEntity<List<Vaga>> getVagasOcupadas(){
		List <Vaga> vagas = service.getVagasOcupadas();
		return ResponseEntity.ok().body(vagas);
	}
	
	//Retorna um inteiro com o número de vagas disponiveis
	@RequestMapping(value="/quantidadedisponiveis", method=RequestMethod.GET)
	public ResponseEntity<Integer> getQtdVagasDisponiveis(){
		Integer qtdVagas = service.getQtdVagasDisponiveis();
		return ResponseEntity.ok().body(qtdVagas); 
	}
	
	//Insere uma nova vaga no sistema
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Void> insert (@RequestBody Vaga vaga){
		Vaga novaVaga = service.insert(vaga);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(novaVaga.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}
	
	//Atualiza manualmente uma vaga
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> update(@RequestBody Vaga vaga){
		service.update(vaga);
		return ResponseEntity.ok().build();
	}
	
	//Busca e deleta uma vaga do sistema através do ID
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}

}
