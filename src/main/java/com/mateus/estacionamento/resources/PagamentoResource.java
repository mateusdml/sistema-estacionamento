package com.mateus.estacionamento.resources;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mateus.estacionamento.domain.Pagamento;
import com.mateus.estacionamento.services.PagamentoService;

@RestController
@RequestMapping(value="faturamento")
public class PagamentoResource {
	
	//Disponibiliza endpoints de pagamento para interação com o sistema
	
	@Autowired
	private PagamentoService service;
	
	//Retorna a lista de pagamentos cadastrados no sistema
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<Pagamento>> findAll() {
		List<Pagamento> pagamentos = service.findAll();
		return ResponseEntity.ok().body(pagamentos);
	}
	
	//Busca um pagamento específico pelo ID
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<Pagamento> find(@PathVariable("id") Integer id){
		return ResponseEntity.ok().body(service.find(id));
	}
	
	//Retorna todos os pagamentos com estado "Pago"
	@RequestMapping(value="/pagos", method=RequestMethod.GET)
	public ResponseEntity<List<Pagamento>> getPagos(){
		List <Pagamento> pagos = service.getPagos();;		
		return ResponseEntity.ok().body(pagos);
	}
	
	//Retorna todos os pagamentos com estado "Pendente"
	@RequestMapping(value="/pendentes", method=RequestMethod.GET)
	public ResponseEntity<List<Pagamento>> getPendentes(){
		List <Pagamento> pendentes = service.getPendentes();;		
		return ResponseEntity.ok().body(pendentes);
	}
	
	//Insere manualmente um novo pagamento no sistema. 
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Void> insert (@RequestBody Pagamento pagamento){
		Pagamento novoPagamento = service.insert(pagamento);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(novoPagamento.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}
	
	//Atualiza manualmente um pagamento
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> update(@RequestBody Pagamento pagamento){
		service.update(pagamento);
		return ResponseEntity.ok().build();
	}
	
	//Busca e deleta um pagamento do sistema através do ID
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	//Retorna todos os pagamentos paginados 
	@RequestMapping(value="/page", method = RequestMethod.GET)
	public ResponseEntity<Page<Pagamento>> findPage(
			@RequestParam(value="page", defaultValue="0") Integer page, 
			@RequestParam(value="linesPerPage", defaultValue="10") Integer linesPerPage, 
			@RequestParam(value="orderBy", defaultValue="id") String orderBy, 
			@RequestParam(value="direction", defaultValue="ASC") String direction) {
		Page<Pagamento> list = service.findPage(page, linesPerPage, orderBy, direction);
		return ResponseEntity.ok().body(list);
	}

}
