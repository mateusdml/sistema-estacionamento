package com.mateus.estacionamento.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mateus.estacionamento.domain.Estacionamento;
import com.mateus.estacionamento.domain.Pagamento;
import com.mateus.estacionamento.domain.Vaga;
import com.mateus.estacionamento.services.EstacionamentoService;
import com.mateus.estacionamento.services.PagamentoService;
import com.mateus.estacionamento.services.VagaService;


@RestController
@RequestMapping(value="estacionamento")
public class EstacionamentoResource {
	
	//Disponibiliza endpoints do estacionamento para interação com o sistema
	
	@Autowired
	private EstacionamentoService service;
	@Autowired
	private VagaService vagaService;
	@Autowired
	private PagamentoService pagamentoService;
	
	//Retorna o estacionamento padrão do sistema (1)
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<Estacionamento> getEstacionamento(){
		Estacionamento obj = service.getEstacionamento();
		return ResponseEntity.ok().body(obj);
	}
	
	//Estaciona um carro em uma vaga disponível
	@RequestMapping(value="estacionar/{id}/{placaCarro}", method=RequestMethod.PUT)
	public ResponseEntity<Void> estacionar(@PathVariable("id") Integer id,@PathVariable("placaCarro") String placaCarro){
		return vagaService.estacionar(id,placaCarro);
	}
	
	//Libera uma vaga e gera um novo ticket para pagamento 
	@RequestMapping(value="vagar/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> vagar(@PathVariable("id") Integer id){
		Vaga vaga = vagaService.find(id);
		if (vaga.getOcupada()==false) {
			return ResponseEntity.badRequest().build();
		}
		Pagamento pag = new Pagamento(vagaService.find(id), service.find(1));
		pagamentoService.insert(pag);
		service.insertPag(pag);
		vagaService.vagar(id);		
		return ResponseEntity.ok().build();
	}
	
	//Realiza o pagamento de um ticket
	@RequestMapping(value="pagar/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> pagar(@PathVariable("id") Integer id){
		Pagamento pag = pagamentoService.find(id);
		System.out.println("Chegou aqui");
		if (pag == null) {
			return ResponseEntity.badRequest().build();
		}
		pagamentoService.pagar(id);
		return ResponseEntity.ok().build();
	}
}
