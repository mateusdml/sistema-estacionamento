package com.mateus.estacionamento.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.mateus.estacionamento.domain.Vaga;
import com.mateus.estacionamento.repositories.VagaRepository;
import com.mateus.estacionamento.services.exceptions.DataIntegrityException;
import com.mateus.estacionamento.services.exceptions.ObjectNotFoundException;

@Service
public class VagaService {

	@Autowired
	private VagaRepository repo;
	
	//Retorna a lista de vagas cadastrados no sistema
	public List<Vaga> findAll() {
		return repo.findAll();
	}
	
	//Busca uma vaga específica pelo ID
	public Vaga find(Integer id) {
		Optional<Vaga> vaga = repo.findById(id);
		return vaga.orElseThrow(() -> new ObjectNotFoundException("Vaga não encontrada"));
	}
	
	//Retorna uma lista com todas as vagas disponíveis
	public List<Vaga> getVagasDisponiveis() {
		List<Vaga> vagas = repo.findAll();
		List<Vaga> vagasDisponiveis = new ArrayList<>();
		for (Vaga i : vagas) {
			if(i.getOcupada()==false) {
				vagasDisponiveis.add(i);
			}
		}
		return vagasDisponiveis;
	}
	
	//Retorna uma lista com todas as vagas ocupadas
	public List<Vaga> getVagasOcupadas() {
		List<Vaga> vagas = repo.findAll();
		List<Vaga> vagasOcupadas = new ArrayList<>();
		for (Vaga i : vagas) {
			if(i.getOcupada()==true) {
				vagasOcupadas.add(i);
			}
		}
		return vagasOcupadas;
	}
	
	//Retorna um inteiro com o número de vagas disponiveis
	public Integer getQtdVagasDisponiveis() {
		List<Vaga> vagasDisponiveis = getVagasDisponiveis();
		return vagasDisponiveis.size();
	}

	//Insere uma nova vaga no sistema
	public Vaga insert(Vaga vaga) {
		vaga.setId(null);
		return repo.save(vaga);
	}
	
	//Ocupa uma vaga
	public ResponseEntity<Void> estacionar(Integer id, String placaCarro) {
		Optional<Vaga> optVaga = repo.findById(id);
		Vaga vaga = optVaga.get();
		if(vaga.getOcupada()==true) {
			return ResponseEntity.badRequest().build();
		}
		vaga.setPlacaCarro(placaCarro);
		vaga.setOcupada(true);
		vaga.setHoraDeEntrada(new java.util.Date());
		repo.save(vaga);
		return ResponseEntity.noContent().build();
	}

	//Libera uma vaga
	public ResponseEntity<Void> vagar(Integer id) {
		Optional<Vaga> optVaga = repo.findById(id);
		Vaga vaga = optVaga.get();
		vaga.setPlacaCarro(null);
		vaga.setOcupada(false);
		vaga.setHoraDeEntrada(null);
		repo.save(vaga);
		return ResponseEntity.noContent().build();		
	}
	
	//Atualiza manualmente uma vaga
	public Vaga update(Vaga vaga) {
		return repo.save(vaga);
	}
	
	//Busca e deleta uma vaga do sistema através do ID
	public void delete(Integer id) {
		find(id);
		try {
		repo.deleteById(id);
		}
		catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possível excluir a vaga");
		}
	}
}
