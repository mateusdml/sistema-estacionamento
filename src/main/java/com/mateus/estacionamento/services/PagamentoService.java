package com.mateus.estacionamento.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.mateus.estacionamento.domain.Pagamento;
import com.mateus.estacionamento.domain.enums.EstadoPagamento;
import com.mateus.estacionamento.repositories.PagamentoRepository;
import com.mateus.estacionamento.services.exceptions.DataIntegrityException;
import com.mateus.estacionamento.services.exceptions.ObjectNotFoundException;

@Service
public class PagamentoService {
	
	//Classe de serviços de pagamento
	
	@Autowired
	private PagamentoRepository repo;
	
	//Busca um pagamento específico pelo ID
	public Pagamento find(Integer id) {
		Optional<Pagamento> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id +", Tipo: " + Pagamento.class.getName()));
	}
	
	//Retorna a lista de pagamentos cadastrados no sistema
	public List<Pagamento> findAll() {
		return repo.findAll();
	}
	
	//Retorna todos os pagamentos com estado "Pago"
	public List<Pagamento> getPagos() {
		List<Pagamento> pagamentos = repo.findAll();
		List<Pagamento> pagamentosPagos = new ArrayList<>();
		for (Pagamento i : pagamentos) {
			if(i.getEstado() == EstadoPagamento.PAGO) {
				pagamentosPagos.add(i);
			}
		}
		return pagamentosPagos;
	}
	
	//Retorna todos os pagamentos com estado "Pendente"
	public List<Pagamento> getPendentes() {
		List<Pagamento> pagamentos = repo.findAll();
		List<Pagamento> pagamentosPendentes = new ArrayList<>();
		for (Pagamento i : pagamentos) {
			if(i.getEstado() == EstadoPagamento.PENDENTE) {
				pagamentosPendentes.add(i);
			}
		}
		return pagamentosPendentes;
	}
	
	//Insere manualmente um novo pagamento no sistema.
	public Pagamento insert(Pagamento pag) {
		pag.setId(null);
		return repo.save(pag);
	}
	
	//Atualiza manualmente um pagamento
	public Pagamento update(Pagamento pag) {
		return repo.save(pag);
	}
	
	//Muda o status de um pagamento de Pendente para Pago
	public ResponseEntity<Void> pagar(Integer id) {
		Optional<Pagamento> optPagamento = repo.findById(id);
		Pagamento pag = optPagamento.get();
		pag.setEstado(EstadoPagamento.PAGO);
		pag.setDataPagamento(new java.util.Date());
		repo.save(pag);
		return ResponseEntity.noContent().build();	
	}

	//Busca e deleta um pagamento do sistema através do ID
	public void delete(Integer id) {
		find(id);
		try {
		repo.deleteById(id);
		}
		catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possível excluir o pagamento");
		}
	}

	//Retorna todos os pagamentos paginados 
	public Page<Pagamento> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return repo.findAll(pageRequest);	
	}
}
