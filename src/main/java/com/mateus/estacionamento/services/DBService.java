package com.mateus.estacionamento.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mateus.estacionamento.domain.Estacionamento;
import com.mateus.estacionamento.domain.Vaga;
import com.mateus.estacionamento.repositories.EstacionamentoRepository;
import com.mateus.estacionamento.repositories.VagaRepository;

@Service
public class DBService {
	
	@Autowired
	private EstacionamentoRepository estacionamentoRepository;
	@Autowired
	private VagaRepository vagaRepository;
	
	public void instantiateTestDatabase() {
		
		List<Vaga> vagas = new ArrayList<>();
		Estacionamento estacionamento = new Estacionamento ();
		estacionamentoRepository.save(estacionamento);
		
		for (int i = 0; i < 10; i++) {
			Vaga vaga = new Vaga(null, estacionamento, ('A'+Integer.toString(i+1)));
			vagaRepository.save(vaga);
			vagas.add(vaga);
		}
		
		for (int i = 0; i < 10; i++) {
			Vaga vaga = new Vaga(null, estacionamento, ('B'+Integer.toString(i+1)));
			vagaRepository.save(vaga);
			vagas.add(vaga);
		}
		estacionamento.setVagas(vagas);	
		estacionamentoRepository.save(estacionamento);	
	}

}
