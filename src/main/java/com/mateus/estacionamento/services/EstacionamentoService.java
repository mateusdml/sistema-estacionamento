package com.mateus.estacionamento.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mateus.estacionamento.domain.Estacionamento;
import com.mateus.estacionamento.domain.Pagamento;
import com.mateus.estacionamento.repositories.EstacionamentoRepository;
import com.mateus.estacionamento.services.exceptions.ObjectNotFoundException;

@Service
public class EstacionamentoService {
	
	//Define qual o Id da instância padrão do estacionamento no banco de dados
	static Integer estacionamentoPadrão = 1;
	
	//Classe de serviços de estacionamento
	
	@Autowired
	private EstacionamentoRepository repo;
	
	//Busca um estacionamento pelo id
	public Estacionamento find(Integer id) {
		Optional<Estacionamento> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id +", Tipo: " + Estacionamento.class.getName()));
	}
	
	//Retorna o estacionamento padrão
	public Estacionamento getEstacionamento() {
		Optional<Estacionamento> obj = repo.findById(estacionamentoPadrão);
		return obj.orElseThrow(() -> new ObjectNotFoundException("Estacionamento não encontrado"));
	}
	
	//Insere um novo estacionamento no Banco de dados
	public Estacionamento insert(Estacionamento obj) {
		obj.setId(null);
		return repo.save(obj);
	}

	//Atualiza um objeto estacionamento no banco de dados
	public Estacionamento update(Estacionamento obj) {
		return repo.save(obj);
	}
	
	//Insere um novo pagamento na lista de pagamentos do estacionamento padrão
	public Estacionamento insertPag(Pagamento pag) {
		Optional<Estacionamento> obj = repo.findById(estacionamentoPadrão);
		Estacionamento estacionamento = obj.get();
		estacionamento.addPagamento(pag);
		return repo.save(estacionamento);
	}
}
