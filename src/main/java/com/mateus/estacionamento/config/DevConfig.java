package com.mateus.estacionamento.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.mateus.estacionamento.services.DBService;

@Configuration
@Profile("dev")
public class DevConfig {

	//Arquivo de configuração para ambiente de desenvolvimento
	@Autowired
	private DBService dbService;
	
	//Pega o valor da variável no application-dev.properties e determina se é necessário povoar o banco de dados
	@Value("${spring.jpa.hibernate.ddl-auto}")
	private String strategy;
	
	@Bean
	public boolean instantiateDatabase() {
		
		if(!"create".equals(strategy)) {
			return false;
		}
		dbService.instantiateTestDatabase();	
		return true;
	}
}
