package com.mateus.estacionamento.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.mateus.estacionamento.services.DBService;

@Configuration
@Profile("test")
public class TestConfig {
	
	//Arquivo de configuração para ambiente de testes
	@Autowired
	private DBService dbService;
	
	@Bean
	public boolean instantiateDatabase() {
		dbService.instantiateTestDatabase();		
		return true;
	}
}
